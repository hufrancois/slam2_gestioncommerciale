
package exercices;

import data.Dao;
import entites.Facture;
import utilitaires.UtilDate;
import static utilitaires.UtilDate.*;


public class B05 {

    public static void main(String[] args) {
        
        System.out.println();
        
        
        System.out.printf("Factures de Janvier 2018 non réglées à la date du %-8s\n\n",aujourdhuiChaine());
        
        for(Facture f : Dao.getToutesLesFactures()){
            
            int anF = UtilDate.annee(f.getDateFact());
            int moisF = UtilDate.mois(f.getDateFact());
            
            if(anF == 2018 && moisF == 1 && !f.getReglee()){
                
                f.affichageConsole();
                System.out.println("");
                
            }
            
            
        }
        
        // A COMPLETER
        
        System.out.println();
    }
}
