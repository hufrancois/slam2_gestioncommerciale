
package data;

import entites.Client;
import entites.Facture;
import entites.LigneDeCommande;
import entites.Produit;
import entites.Region;
import java.util.LinkedList;
import java.util.List;
import static utilitaires.UtilDate.chaineVersDate;

public class Donnees {
     
    static   private  List<Region>    toutesLesRegions  = new LinkedList<Region>(); 
    static   private  List<Client>    tousLesClients    = new LinkedList<Client>();
    static   private  List<Facture>   toutesLesFactures = new LinkedList<Facture>();
    static   private  List<Produit>   tousLesProduits   = new LinkedList<Produit>();

    //<editor-fold defaultstate="collapsed" desc="CODE STATIQUE INITIALISANT LES DONNEES">
    
    static{
        
        
        Region  r1=new Region();
        r1.setCodeRegion("HDF");
        r1.setNomRegion("Hauts de France");
        
        Region  r2=new Region();
        r2.setCodeRegion("GE");
        r2.setNomRegion("Grand Est");
        
        toutesLesRegions.add(r1);toutesLesRegions.add(r2);
        
        Client c1= new Client();
        c1.setNumCli(101L);
        c1.setNomCli("Dupont");
        c1.setAdrCli("Arras");
        
        c1.setLaRegion(r1);
        r1.getLesClients().add(c1);
        
        tousLesClients.add(c1);
        
        Client c2= new Client();
        c2.setNumCli(102L);
        c2.setNomCli("Durant");
        c2.setAdrCli("Lille");
        
        c2.setLaRegion(r1);
        r1.getLesClients().add(c2);
        
        tousLesClients.add(c2);
        
        Client c3= new Client();
        c3.setNumCli(103L);
        c3.setNomCli("Leroy");
        c3.setAdrCli("Hirson");
        
        
        c3.setLaRegion(r2);
        r2.getLesClients().add(c3);
        
        tousLesClients.add(c3);
        
  ////////////////////////////////////////////////////////////////  FACTURES //////////////////////////////////////////////////////////////////
        
        
        
        
        Facture f11=new Facture();
        f11.setNumFact(911L);
        f11.setDateFact(chaineVersDate("11/12/2017"));
        
        f11.setReglee(true);
        f11.setLeClient(c1);
        
        toutesLesFactures.add(f11);
        c1.getLesFactures().add(f11);
        
        
        Facture f12=new Facture();
        f12.setNumFact(912L);
        f12.setDateFact(chaineVersDate("11/12/2017"));
       
        f12.setReglee(true);
        f12.setLeClient(c2);
        
        toutesLesFactures.add(f12);
        c2.getLesFactures().add(f12);
        
        Facture f13=new Facture();
        f13.setNumFact(913L);
        f13.setDateFact(chaineVersDate("14/12/2017"));
       
        f13.setReglee(true);
        f13.setLeClient(c2);
        
        toutesLesFactures.add(f13);
        c2.getLesFactures().add(f13);
        
        Facture f14=new Facture();
        f14.setNumFact(914L);
        f14.setDateFact(chaineVersDate("20/12/2017"));
       
        f14.setReglee(true);
        f14.setLeClient(c3);
        
        toutesLesFactures.add(f14);
        c3.getLesFactures().add(f14);
        
        
        Facture f15=new Facture();
        f15.setNumFact(915L);
        f15.setDateFact(chaineVersDate("22/12/2017"));
       
        f15.setReglee(true);
        f15.setLeClient(c1);
        
        toutesLesFactures.add(f15);
        c1.getLesFactures().add(f15);
        
       
        
        
        
        Facture f01=new Facture();
        
        f01.setNumFact(1001L);
        f01.setDateFact(chaineVersDate("02/01/2018"));
       
        f01.setReglee(true);
        f01.setLeClient(c1);
        
        toutesLesFactures.add(f01);
        c1.getLesFactures().add(f01);
        
        
        Facture f02=new Facture();
        f02.setNumFact(1002L);
        f02.setDateFact(chaineVersDate("02/01/2018"));
       
        f02.setReglee(true);
        f02.setLeClient(c2);
        
        toutesLesFactures.add(f02);
        c2.getLesFactures().add(f02);
        
        
        Facture f03=new Facture();
        f03.setNumFact(1003L);
        f03.setDateFact(chaineVersDate("03/01/2018"));
       
        f03.setReglee(true);
        f03.setLeClient(c1);
        
        toutesLesFactures.add(f03);
        c1.getLesFactures().add(f03);
        
        
        Facture f04=new Facture();
        f04.setNumFact(1004L);
        f04.setDateFact(chaineVersDate("03/01/2018"));
        
        f04.setReglee(true);
        f04.setLeClient(c3);
        
        toutesLesFactures.add(f04);
        c3.getLesFactures().add(f04);
        
        
        Facture f05=new Facture();
        f05.setNumFact(1005L);
        f05.setDateFact(chaineVersDate("04/01/2018"));
       
        f05.setReglee(true);
        f05.setLeClient(c2);
        
        toutesLesFactures.add(f05);
        c2.getLesFactures().add(f05);
        
        
        Facture f06=new Facture();
        f06.setNumFact(1006L);
        f06.setDateFact(chaineVersDate("05/01/2018"));
        
        f06.setReglee(true);
        f06.setLeClient(c3);
        
        toutesLesFactures.add(f06);
        c3.getLesFactures().add(f06);
        
        Facture f07=new Facture();
        f07.setNumFact(1007L);
        f07.setDateFact(chaineVersDate("06/01/2018"));
       
        f07.setReglee(false);
        f07.setLeClient(c2);
        
        toutesLesFactures.add(f07);
        c2.getLesFactures().add(f07);
        
        
        Facture f08=new Facture();
        f08.setNumFact(1008L);
        f08.setDateFact(chaineVersDate("08/01/2018"));
      
        f08.setReglee(false);
        f08.setLeClient(c1);
        
        toutesLesFactures.add(f08);
        c1.getLesFactures().add(f08);
        
        
        Facture f09=new Facture();
        f09.setNumFact(1009L);
        f09.setDateFact(chaineVersDate("08/01/2018"));
        
        f09.setReglee(false);
        f09.setLeClient(c3);
        
        toutesLesFactures.add(f09);
        c3.getLesFactures().add(f09);
        
        
        
        Facture f10=new Facture();
        f10.setNumFact(1010L);
        f10.setDateFact(chaineVersDate("09/01/2018"));
       
        f10.setReglee(false);
        f10.setLeClient(c1);
        
        toutesLesFactures.add(f10);
        c1.getLesFactures().add(f10);
        
        
        
        Facture f21=new Facture();
        f21.setNumFact(1021L);
        f21.setDateFact(chaineVersDate("01/02/2018"));
       
        f21.setReglee(false);
        f21.setLeClient(c1);
        
        toutesLesFactures.add(f21);
        c1.getLesFactures().add(f21);
        
        Facture f22=new Facture();
        f22.setNumFact(1022L);
        f22.setDateFact(chaineVersDate("01/02/2018"));
      
        f22.setReglee(true);
        f22.setLeClient(c2);
        
        toutesLesFactures.add(f22);
        c2.getLesFactures().add(f22);
        
        
        Facture f23=new Facture();
        f23.setNumFact(1023L);
        f23.setDateFact(chaineVersDate("02/02/2018"));
       
        f23.setReglee(false);
        f23.setLeClient(c2);
        
        toutesLesFactures.add(f23);
        c2.getLesFactures().add(f23);
        
        
        Facture f24=new Facture();
        f24.setNumFact(1024L);
        f24.setDateFact(chaineVersDate("02/02/2018"));
       
        f24.setReglee(true);
        f24.setLeClient(c3);
        
        toutesLesFactures.add(f24);
        c3.getLesFactures().add(f24);
        
        
        Facture f25=new Facture();
        f25.setNumFact(1025L);
        f25.setDateFact(chaineVersDate("02/02/2018"));
       
        f25.setReglee(false);
        f25.setLeClient(c3);
        
        toutesLesFactures.add(f25);
        c3.getLesFactures().add(f25);
        
        
        
        /////////////////////////////////////////////////////////////////////////////////////////////// 
        
        
        Produit pra= new Produit();
        pra.setCodeProd("PRA"); pra.setDesigProd("Produit A");pra.setPrixProd(75.5F);
        
        Produit prb= new Produit();
        prb.setCodeProd("PRB"); prb.setDesigProd("Produit B");prb.setPrixProd(132.75F);
        
        Produit prc= new Produit();
        prc.setCodeProd("PRC"); prc.setDesigProd("Produit C");prc.setPrixProd(88.3F);
        
        Produit prd= new Produit();
        prd.setCodeProd("PRD"); prd.setDesigProd("Produit D");prd.setPrixProd(218.3F);
        
        Produit pre= new Produit();
        pre.setCodeProd("PRE"); pre.setDesigProd("Produit E");pre.setPrixProd(55.25F);
        
        Produit prf= new Produit();
        prf.setCodeProd("PRF"); prf.setDesigProd("Produit F");prf.setPrixProd(238.99F);
        
        
        tousLesProduits.add(pra); tousLesProduits.add(prb);tousLesProduits.add(prc);
        tousLesProduits.add(prd);tousLesProduits.add(pre); tousLesProduits.add(prf);
        
        
        /////////////////////////////////////////////////////////////////////////////////////////////////
        
        
        LigneDeCommande  lgc0101=new LigneDeCommande();
        lgc0101.setLaFacture(f01); lgc0101.setLeProduit(prb);lgc0101.setQuantite(2);
        f01.getLesLigneDeCommandes().add(lgc0101);
        prb.getLesLigneDeCommandes().add(lgc0101);
        
        
        LigneDeCommande  lgc0102=new LigneDeCommande();
        lgc0102.setLaFacture(f01); lgc0102.setLeProduit(prd);lgc0102.setQuantite(1);
         
        f01.getLesLigneDeCommandes().add(lgc0102);
        prb.getLesLigneDeCommandes().add(lgc0102);
        
        LigneDeCommande  lgc0103=new LigneDeCommande();
        lgc0103.setLaFacture(f01); lgc0103.setLeProduit(pre);lgc0103.setQuantite(3);
        
        f01.getLesLigneDeCommandes().add(lgc0103);
        prb.getLesLigneDeCommandes().add(lgc0103);
        
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        
        
        LigneDeCommande  lgc0201=new LigneDeCommande();
        lgc0201.setLaFacture(f02); lgc0201.setLeProduit(pra);lgc0201.setQuantite(1);
        
        f02.getLesLigneDeCommandes().add(lgc0201);
        pra.getLesLigneDeCommandes().add(lgc0201);
        
        
        LigneDeCommande  lgc0202=new LigneDeCommande();
        lgc0202.setLaFacture(f02); lgc0202.setLeProduit(prd);lgc0202.setQuantite(2);
        
        f02.getLesLigneDeCommandes().add(lgc0202);
        prd.getLesLigneDeCommandes().add(lgc0202);
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        
        
        LigneDeCommande  lgc0301=new LigneDeCommande();
        lgc0301.setLaFacture(f03); lgc0301.setLeProduit(pre);lgc0301.setQuantite(2);
        
        f03.getLesLigneDeCommandes().add(lgc0301);
        pre.getLesLigneDeCommandes().add(lgc0301);
        
        LigneDeCommande  lgc0302=new LigneDeCommande();
        lgc0302.setLaFacture(f03); lgc0302.setLeProduit(prf);lgc0302.setQuantite(1);
        
        
        f03.getLesLigneDeCommandes().add(lgc0302);
        prf.getLesLigneDeCommandes().add(lgc0302);
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        
        
        LigneDeCommande  lgc0401=new LigneDeCommande();
        lgc0401.setLaFacture(f04); lgc0401.setLeProduit(prb);lgc0401.setQuantite(3);
        
        f04.getLesLigneDeCommandes().add(lgc0401);
        prb.getLesLigneDeCommandes().add(lgc0401);
        
        LigneDeCommande  lgc0402=new LigneDeCommande();
        lgc0402.setLaFacture(f04); lgc0402.setLeProduit(pre);lgc0402.setQuantite(2);
        
        f04.getLesLigneDeCommandes().add(lgc0402);
        pre.getLesLigneDeCommandes().add(lgc0402);
        
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        
        
         
        LigneDeCommande  lgc0501=new LigneDeCommande();
        lgc0501.setLaFacture(f05); lgc0501.setLeProduit(prf);lgc0501.setQuantite(1);
        
        f05.getLesLigneDeCommandes().add(lgc0501);
        prf.getLesLigneDeCommandes().add(lgc0501);
        
        LigneDeCommande  lgc0502=new LigneDeCommande();
        lgc0502.setLaFacture(f05); lgc0502.setLeProduit(pra);lgc0502.setQuantite(1);
        
        f05.getLesLigneDeCommandes().add(lgc0502);
        pra.getLesLigneDeCommandes().add(lgc0502);
        
        LigneDeCommande  lgc0503=new LigneDeCommande();
        lgc0503.setLaFacture(f05); lgc0503.setLeProduit(prc);lgc0503.setQuantite(1);
        
        f05.getLesLigneDeCommandes().add(lgc0503);
        prc.getLesLigneDeCommandes().add(lgc0503);
        
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        
        LigneDeCommande  lgc0601=new LigneDeCommande();
        lgc0601.setLaFacture(f06); lgc0601.setLeProduit(prf);lgc0601.setQuantite(1);
        
        f06.getLesLigneDeCommandes().add(lgc0601);
        prf.getLesLigneDeCommandes().add(lgc0601);
        
        LigneDeCommande  lgc0602=new LigneDeCommande();
        lgc0602.setLaFacture(f06); lgc0602.setLeProduit(prc);lgc0602.setQuantite(2);
        
        f06.getLesLigneDeCommandes().add(lgc0602);
        prc.getLesLigneDeCommandes().add(lgc0602);
       
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        
        
        LigneDeCommande  lgc0701=new LigneDeCommande();
        lgc0701.setLaFacture(f07); lgc0701.setLeProduit(pre);lgc0701.setQuantite(3);
        
        f07.getLesLigneDeCommandes().add(lgc0701);
        pre.getLesLigneDeCommandes().add(lgc0701);
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        
        LigneDeCommande  lgc0801=new LigneDeCommande();
        lgc0801.setLaFacture(f08); lgc0801.setLeProduit(pra);lgc0801.setQuantite(4);
        
        f08.getLesLigneDeCommandes().add(lgc0801);
        pra.getLesLigneDeCommandes().add(lgc0801);
        
        
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////
         
        
        LigneDeCommande  lgc0901=new LigneDeCommande();
        lgc0901.setLaFacture(f09); lgc0901.setLeProduit(pre);lgc0901.setQuantite(2);
             
        f09.getLesLigneDeCommandes().add(lgc0901);
        pre.getLesLigneDeCommandes().add(lgc0901);
        
        
        LigneDeCommande  lgc0902=new LigneDeCommande();
        lgc0902.setLaFacture(f09); lgc0902.setLeProduit(pra);lgc0902.setQuantite(3);
        
        f09.getLesLigneDeCommandes().add(lgc0902);
        pra.getLesLigneDeCommandes().add(lgc0902);
        
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
          
        LigneDeCommande  lgc1001=new LigneDeCommande();
        lgc1001.setLaFacture(f10); lgc1001.setLeProduit(prb);lgc1001.setQuantite(1);
        
        f10.getLesLigneDeCommandes().add(lgc1001);
        prb.getLesLigneDeCommandes().add(lgc1001);
        
        
        LigneDeCommande  lgc1002=new LigneDeCommande();
        lgc1002.setLaFacture(f10); lgc1002.setLeProduit(prd);lgc1002.setQuantite(2);
        
        f10.getLesLigneDeCommandes().add(lgc1002);
        prd.getLesLigneDeCommandes().add(lgc1002);
        
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
          
        LigneDeCommande  lgc2101=new LigneDeCommande();
        lgc2101.setLaFacture(f21); lgc2101.setLeProduit(pra);lgc2101.setQuantite(2);
        
        f21.getLesLigneDeCommandes().add(lgc2101);
        pra.getLesLigneDeCommandes().add(lgc2101);
        
        LigneDeCommande  lgc2102=new LigneDeCommande();
        lgc2102.setLaFacture(f21); lgc2102.setLeProduit(pre);lgc2102.setQuantite(3);
        
        
        f21.getLesLigneDeCommandes().add(lgc2102);
        pre.getLesLigneDeCommandes().add(lgc2102);
        
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        
         LigneDeCommande  lgc2201=new LigneDeCommande();
         lgc2201.setLaFacture(f22); lgc2201.setLeProduit(prb);lgc2201.setQuantite(1);
        
         f22.getLesLigneDeCommandes().add(lgc2201);
         prb.getLesLigneDeCommandes().add(lgc2201);
         
         
         LigneDeCommande  lgc2202=new LigneDeCommande();
         lgc2202.setLaFacture(f22); lgc2202.setLeProduit(prc);lgc2202.setQuantite(3);
         
         f22.getLesLigneDeCommandes().add(lgc2202);
         prc.getLesLigneDeCommandes().add(lgc2202);
         
        
         LigneDeCommande  lgc2203=new LigneDeCommande();
         lgc2203.setLaFacture(f22); lgc2203.setLeProduit(pre);lgc2203.setQuantite(2);
         
         f22.getLesLigneDeCommandes().add(lgc2203);
         pre.getLesLigneDeCommandes().add(lgc2203);

         
         ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
         
         LigneDeCommande  lgc2301=new LigneDeCommande();
         lgc2301.setLaFacture(f23); lgc2301.setLeProduit(prc);lgc2301.setQuantite(2);
         
         
         f23.getLesLigneDeCommandes().add(lgc2301);
         prc.getLesLigneDeCommandes().add(lgc2301);
        
         LigneDeCommande  lgc2302=new LigneDeCommande();
         lgc2302.setLaFacture(f23); lgc2302.setLeProduit(prd);lgc2302.setQuantite(1);
         
         f23.getLesLigneDeCommandes().add(lgc2302);
         prd.getLesLigneDeCommandes().add(lgc2302);
         
         
         //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
         
         
         LigneDeCommande  lgc2401=new LigneDeCommande();
         lgc2401.setLaFacture(f24); lgc2401.setLeProduit(prc);lgc2401.setQuantite(1);
              
         f24.getLesLigneDeCommandes().add(lgc2401);
         prc.getLesLigneDeCommandes().add(lgc2401);
         
        
         LigneDeCommande  lgc2402=new LigneDeCommande();
         lgc2402.setLaFacture(f24); lgc2402.setLeProduit(prf);lgc2402.setQuantite(3);

         
          f24.getLesLigneDeCommandes().add(lgc2402);
          prf.getLesLigneDeCommandes().add(lgc2402);
         
         
         /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
         
         
         
         LigneDeCommande  lgc2501=new LigneDeCommande();
         lgc2501.setLaFacture(f25); lgc2501.setLeProduit(prd);lgc2501.setQuantite(2);
         
         f25.getLesLigneDeCommandes().add(lgc2501);
         prd.getLesLigneDeCommandes().add(lgc2501);
        
         LigneDeCommande  lgc2502=new LigneDeCommande();
         lgc2502.setLaFacture(f25); lgc2502.setLeProduit(pre);lgc2502.setQuantite(2);
         
         f25.getLesLigneDeCommandes().add(lgc2502);
         pre.getLesLigneDeCommandes().add(lgc2502);

         //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
         
         
         LigneDeCommande  lgc1101=new LigneDeCommande();
         lgc1101.setLaFacture(f11); lgc1101.setLeProduit(pra);lgc1101.setQuantite(3);
         
         
         f11.getLesLigneDeCommandes().add(lgc1101);
         pra.getLesLigneDeCommandes().add(lgc1101);

        
         LigneDeCommande  lgc1102=new LigneDeCommande();
         lgc1102.setLaFacture(f11); lgc1102.setLeProduit(prd);lgc1102.setQuantite(1);
         
         f11.getLesLigneDeCommandes().add(lgc1102);
         prd.getLesLigneDeCommandes().add(lgc1102);
         
         
         LigneDeCommande  lgc1103=new LigneDeCommande();
         lgc1103.setLaFacture(f11); lgc1103.setLeProduit(prf);lgc1103.setQuantite(2);
         
         f11.getLesLigneDeCommandes().add(lgc1103);
         prf.getLesLigneDeCommandes().add(lgc1103);
         
         ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////     
         
         LigneDeCommande  lgc1201=new LigneDeCommande();
         lgc1201.setLaFacture(f12); lgc1201.setLeProduit(pra);lgc1201.setQuantite(2);
         
         
         f12.getLesLigneDeCommandes().add(lgc1201);
         pra.getLesLigneDeCommandes().add(lgc1201);
         
        
         LigneDeCommande  lgc1202=new LigneDeCommande();
         lgc1202.setLaFacture(f12); lgc1202.setLeProduit(prc);lgc1202.setQuantite(2);
         
         f12.getLesLigneDeCommandes().add(lgc1202);
         prc.getLesLigneDeCommandes().add(lgc1202);
         
         
         ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
         
         LigneDeCommande  lgc1301=new LigneDeCommande();
         lgc1301.setLaFacture(f13); lgc1301.setLeProduit(prc);lgc1301.setQuantite(2);
        
         f13.getLesLigneDeCommandes().add(lgc1301);
         prc.getLesLigneDeCommandes().add(lgc1301);
         
         LigneDeCommande  lgc1302=new LigneDeCommande();
         lgc1302.setLaFacture(f13); lgc1302.setLeProduit(pre);lgc1302.setQuantite(1);
         
         
         f13.getLesLigneDeCommandes().add(lgc1302);
         pre.getLesLigneDeCommandes().add(lgc1302);
         
         //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
         
         
         LigneDeCommande  lgc1401=new LigneDeCommande();
         lgc1401.setLaFacture(f14); lgc1401.setLeProduit(pra);lgc1401.setQuantite(1);
         
         f14.getLesLigneDeCommandes().add(lgc1401);
         pra.getLesLigneDeCommandes().add(lgc1401);
        
         LigneDeCommande  lgc1402=new LigneDeCommande();
         lgc1402.setLaFacture(f14); lgc1402.setLeProduit(prb);lgc1402.setQuantite(3);
         
         f14.getLesLigneDeCommandes().add(lgc1402);
         prb.getLesLigneDeCommandes().add(lgc1402);
         
         LigneDeCommande  lgc1403=new LigneDeCommande();
         lgc1403.setLaFacture(f14); lgc1403.setLeProduit(pre);lgc1403.setQuantite(2);

         f14.getLesLigneDeCommandes().add(lgc1403);
         pre.getLesLigneDeCommandes().add(lgc1403);
         
         /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
         
         LigneDeCommande  lgc1501=new LigneDeCommande();
         lgc1501.setLaFacture(f15); lgc1501.setLeProduit(pre);lgc1501.setQuantite(2);
         
         
          f15.getLesLigneDeCommandes().add(lgc1501);
          pre.getLesLigneDeCommandes().add(lgc1501);
        
          LigneDeCommande  lgc1502=new LigneDeCommande();
          lgc1502.setLaFacture(f15); lgc1502.setLeProduit(prf);lgc1502.setQuantite(1);
         
          f15.getLesLigneDeCommandes().add(lgc1502);
          prf.getLesLigneDeCommandes().add(lgc1502); 
         
    }
    
    //</editor-fold>
    
    
   
    //<editor-fold defaultstate="collapsed" desc="GETTERS">
    
    
    public static List<Region> getToutesLesRegions() {
        return toutesLesRegions;
    }
    
    public static List<Client> getTousLesClients() {
        return tousLesClients;
    }
    
    public static List<Facture> getToutesLesFactures() {
        return toutesLesFactures;
    }
    
    
    public static List<Produit> getTousLesProduits() {
        return tousLesProduits;
    }
    
    //</editor-fold>
}


