package data;
import entites.Client;
import entites.Facture;
import entites.Produit;
import entites.Region;
import java.util.List;

public class Dao {
    
    public static Region           getRegionDeCode(String pCodeReg)  {
    
        Region reg=null;
        
        for (Region r : Donnees.getToutesLesRegions()){
        
            if (r.getCodeRegion().equals(pCodeReg)){reg=r;break;}
            
        }
        
        return reg;
    
    
    }
    public static List<Region>     getToutesLesRegions()             {
        return Donnees.getToutesLesRegions();
    }
        
    public static  Client          getClientDeNumero(Long pNumCli )  {
         
        Client  cli=null;
     
        for(Client  c: Donnees.getTousLesClients()){
         
         if( c.getNumCli().equals(pNumCli) ){ cli=c;break;}
        }
        return cli;
    }
    public static List<Client>     getTousLesClients()               {
         
        
        return Donnees.getTousLesClients();
    
    }
   
    public static Facture          getFactureDeNumero(Long pNumFact) {
    
        Facture fact=null;
        
        
        for (Facture f : Donnees.getToutesLesFactures()){
        
           if(f.getNumFact().equals(pNumFact)){fact=f;break;} 
        }
        
        return fact;

    }
    public static List<Facture>    getToutesLesFactures()            { 
        
        return Donnees.getToutesLesFactures(); }   
    
    
    
    public static Produit getLeProduitDeCode(String pCodeProd){
    
    
      Produit prod=null;
        
        
        for (Produit p : Donnees.getTousLesProduits()){
        
           if(p.getCodeProd().equals(pCodeProd)){prod=p;break;} 
        }
        
        return prod;

    
    
    }
   
    
    public static List<Produit> getTousLesProduits(){
    
    
           return Donnees.getTousLesProduits();
    
    
    }
    
}
