package essaidao;

import data.Dao;
import entites.Produit;

public class EssaiDaoProduit2 {

    public static void main(String[] args) {
                
       
       for (Produit p : Dao.getTousLesProduits()) {
          
            System.out.printf("Produit:  %s %s %8.2f €\n" , p.getCodeProd(),p.getDesigProd(),p.getPrixProd());
       }
       
          
     
        System.out.println();
    }
}