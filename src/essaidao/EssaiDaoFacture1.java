
package essaidao;

import data.Dao;
import entites.Facture;
import utilitaires.UtilDate;
public class EssaiDaoFacture1 {

    public static void main(String[] args) {
        
       Facture f= Dao.getFactureDeNumero(1001L);
       
       System.out.printf( "\n%5d %-10s %-15s  %-20s %10.2f € %-10s\n",
                          f.getNumFact(),
                          UtilDate.dateVersChaine(f.getDateFact()),
                          f.getLeClient().getNomCli(),
                          f.getLeClient().getAdrCli(),
                          f.montantFact(),
                          f.getReglee()?"Réglée":"Non réglée"
                         );
        
        System.out.println();
    }
}
