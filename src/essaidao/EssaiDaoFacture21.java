
package essaidao;

import data.Dao;
import entites.Facture;
import entites.LigneDeCommande;
import java.util.Scanner;

public class EssaiDaoFacture21 {

  
    public static void main(String[] args) {
      
        
        Scanner clavier= new Scanner(System.in);
        
        Long numfact;
        
        
        System.out.println("N° facture? ");
        
        numfact=clavier.nextLong();
        
        Facture f= Dao.getFactureDeNumero(numfact);
        
        
        System.out.println();
        System.out.print("Facture du: "+ utilitaires.UtilDate.dateVersChaine(f.getDateFact()));
        System.out.print("  Client: "+f.getLeClient().getNomCli());
        System.out.print("  Ville: "+f.getLeClient().getAdrCli());
        System.out.println("  Région: "+f.getLeClient().getLaRegion().getNomRegion());
        System.out.println();
        
        Float total=0f;
        
        for (LigneDeCommande lgc : f.getLesLigneDeCommandes()){
        
        
            System.out.printf("%-15s %8.2f €  x%3d % 8.2f €\n",
                    
                    lgc.getLeProduit().getDesigProd(),
                    lgc.getLeProduit().getPrixProd(),
                    lgc.getQuantite(),
                    lgc.getLeProduit().getPrixProd()*   lgc.getQuantite()     
                    
            );
            
            total+=lgc.getLeProduit().getPrixProd()*   lgc.getQuantite() ;
        
        }
        
        System.out.println();
        
        System.out.printf("                         Total:%10.2f €\n\n",total);
    }
}
