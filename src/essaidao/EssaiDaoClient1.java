package essaidao;
import data.Dao;
import entites.Client;
import entites.Facture;
import utilitaires.UtilDate;

public class EssaiDaoClient1 {

    
    public static void main(String[] args) {
        
        
        Client c
                
                = Dao.getClientDeNumero(101L);

        System.out.println();
        
        System.out.println(" Client: " +c.getNumCli());
        System.out.println(" Nom: "    +c.getNomCli());
        System.out.println(" Adresse:" +c.getAdrCli());
        System.out.println(" Region: " +c.getLaRegion().getNomRegion());
        
        System.out.println();
        
        for ( Facture f : c.getLesFactures()){
        
            
            System.out.printf( "%5d %-10s %10.2f € %-10s\n",
                               f.getNumFact(),
                               UtilDate.dateVersChaine(f.getDateFact()),
                               f.montantFact(),
                               f.getReglee()?"Réglée":"Non réglée"
                             );
        }
        
        System.out.println();
    }
}
