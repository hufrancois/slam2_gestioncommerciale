package essaidao;
import data.Dao;
import entites.Client;

public class EssaiCaClientAnnee {

    
    public static void main(String[] args) {
        
        
        Client c = Dao.getClientDeNumero(101L);

        System.out.println();
        
        System.out.println(" Client: " +c.getNumCli());
        System.out.println(" Nom: "    +c.getNomCli());
        System.out.println(" Adresse:" +c.getAdrCli());
        System.out.println(" Region: " +c.getLaRegion().getNomRegion());
        
        System.out.println();
        
         System.out.println("Chiffre d'affaires du client en 2018: "+c.caClient(2018)+" €");
        System.out.println();
    }
}
