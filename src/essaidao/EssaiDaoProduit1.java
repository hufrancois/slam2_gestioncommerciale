package essaidao;

import data.Dao;
import entites.Produit;

public class EssaiDaoProduit1 {

    public static void main(String[] args) {
                
        Produit p= Dao.getLeProduitDeCode("PRA");
        
        System.out.println();
        
        System.out.printf("Produit:  %s %s %8.2f €\n" , p.getCodeProd(),p.getDesigProd(),p.getPrixProd());
     
        System.out.println();
    }
}