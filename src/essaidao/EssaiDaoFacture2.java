
package essaidao;

import data.Dao;
import entites.Facture;
import utilitaires.UtilDate;

public class EssaiDaoFacture2 {

  
    public static void main(String[] args) {
      
        
        System.out.println();
        
        for (Facture f :  Dao.getToutesLesFactures()){
        
         
              System.out.printf( "%5d %-10s %-15s  %-20s %10.2f € %-10s\n",
                               f.getNumFact(),
                               UtilDate.dateVersChaine(f.getDateFact()),
                               f.getLeClient().getNomCli(),
                                f.getLeClient().getAdrCli(),
                               f.montantFact(),
                               f.getReglee()?"Réglée":"Non réglée"
                             );
        
        }
        
        System.out.println();
        
    }
}
