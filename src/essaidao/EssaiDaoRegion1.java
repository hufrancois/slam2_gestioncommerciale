package essaidao;

import data.Dao;
import entites.Client;
import entites.Region;

public class EssaiDaoRegion1 {

    public static void main(String[] args) {
                
        Region r= Dao.getRegionDeCode("HDF");
        
        System.out.println();
        
        System.out.printf("Région:  %s %s %3d\n" , r.getCodeRegion(),r.getNomRegion(),r.getLesClients().size());
        
        
        System.out.println();
        
        for (Client c : r.getLesClients()){
        
             System.out.printf("%5d %-20s %-25s\n", c.getNumCli(), c.getNomCli(),c.getAdrCli());
        }
        
        System.out.println();
    }
}