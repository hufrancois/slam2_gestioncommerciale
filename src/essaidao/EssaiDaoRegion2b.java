package essaidao;

import data.Dao;
import entites.Client;
import entites.Region;

public class EssaiDaoRegion2b {

    public static void main(String[] args) {
                
        Region r= Dao.getRegionDeCode("HDF");
        
      
        r.affichageConsole();
        
        
        System.out.println("\n");
        
        for (Client c : r.getLesClients()){
        
             System.out.printf("%5d %-20s %-25s CA: %8.2f €\n", c.getNumCli(), c.getNomCli(),c.getAdrCli(),c.caClient(2018));
        }
        
        System.out.println();
    }
}