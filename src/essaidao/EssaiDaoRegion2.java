
package essaidao;

import data.Dao;
import entites.Region;

public class EssaiDaoRegion2 {

    public static void main(String[] args) {
        
       
        System.out.println();
        for (Region r : Dao.getToutesLesRegions()){
        
        
             System.out.printf("%-6s  %-30s\n",r.getCodeRegion(),r.getNomRegion());
        
        }
                
        System.out.println();
    }
}
