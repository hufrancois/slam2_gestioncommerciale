
package entites;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import static utilitaires.UtilDate.annee;

public class Produit {

     private String codeProd;
     private String desigProd;
     private Float  prixProd;

     List<LigneDeCommande> lesLigneDeCommandes= new LinkedList();
     
     
     
     
     

   
     
      public Float caProduit (int pAnnee){
    
    
       Float total=0F; 
       
       for(LigneDeCommande lc : this.lesLigneDeCommandes){
           
           Date datefact = lc.getLaFacture().getDateFact();
           int anLC = utilitaires.UtilDate.annee(datefact);
           if(anLC==pAnnee && lc.getLaFacture().getReglee()){
               
               total+=prixProd*lc.getQuantite();
           }
       }
       // A completer
    
       return total;
    }
     
     
     
     //<editor-fold defaultstate="collapsed" desc="gets et sets">
     
     public String getCodeProd() {
         return codeProd;
     }
     
     public void setCodeProd(String codeProd) {
         this.codeProd = codeProd;
     }
     
     public String getDesigProd() {
         return desigProd;
     }
     
     public void setDesigProd(String desigProd) {
         this.desigProd = desigProd;
     }
     
     public Float getPrixProd() {
         return prixProd;
     }
     
     public void setPrixProd(Float prixProd) {
         this.prixProd = prixProd;
     }
     
     
      public List<LigneDeCommande> getLesLigneDeCommandes() {
        return lesLigneDeCommandes;
    }
     //</editor-fold> 
}
