
package entites;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import static utilitaires.UtilDate.dateVersChaine;

public class Facture {
    
    private Long      numFact;
    private Date      dateFact;   
    private Float     montantFact;
    private Boolean   reglee;
    
    private Client    leClient;
    
    
    
    private List<LigneDeCommande> lesLigneDeCommandes=new LinkedList();
    
    public void affichageConsole(){
    
    
     System.out.printf(   "%5d %-10s %-15s  %-15s %-15s %10.2f %-8s €",
                          numFact,
                          dateVersChaine(dateFact),
                          leClient.getNomCli(),
                          leClient.getAdrCli(),
                          leClient.getLaRegion().getNomRegion(),
                          montantFact(),
                          reglee?"Réglée":"Non réglée"
                      );

    }
    
    
    public Float  montantFact(){
    
        Float total=0F;
        
        for(LigneDeCommande lc : lesLigneDeCommandes){
            total+=
        }
        // A Completer
         
        return total;
    } 
    
     
    //<editor-fold defaultstate="collapsed" desc="CONSTRUCTEURS">
    
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="GETTERS & SETTERS">
    
    public Long getNumFact() {
        return numFact;
    }
    
    public void setNumFact(Long numFact) {
        this.numFact = numFact;
    }
    
    
    
    
   public Date getDateFact() {
        return dateFact;
    }

    public void setDateFact(Date dateFact) {
        this.dateFact = dateFact;
    }

    public Boolean getReglee() {
        return reglee;
    }

    public void setReglee(Boolean reglee) {
        this.reglee = reglee;
    }

    public Client getLeClient() {
        return leClient;
    }

    public void setLeClient(Client leClient) {
        this.leClient = leClient;
    }
   
    
    //</editor-fold>
    public List<LigneDeCommande> getLesLigneDeCommandes() {
        return lesLigneDeCommandes;
    }
    
    
}
