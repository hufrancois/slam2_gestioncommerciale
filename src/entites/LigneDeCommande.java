package entites;

public class LigneDeCommande {

    private Facture  laFacture;
    private Produit  leProduit;
    
    private int      quantite;  

    //<editor-fold defaultstate="collapsed" desc="gets et sets">
    
    public Facture getLaFacture() {
        return laFacture;
    }
    
    public void setLaFacture(Facture laFacture) {
        this.laFacture = laFacture;
    }
    
    public Produit getLeProduit() {
        return leProduit;
    }
    
    public void setLeProduit(Produit leProduit) {
        this.leProduit = leProduit;
    }
    
    public int getQuantite() {
        return quantite;
    }
    
    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }
    
    //</editor-fold>
}
